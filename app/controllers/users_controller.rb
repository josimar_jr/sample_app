class UsersController < ApplicationController
  before_action :logged_in_user,  only: [:index, :edit, :update, :following, :followers]
  before_action :correct_user,    only: [:edit, :update]
  before_action :admin_user,      only: :destroy

  def new
    @user = User.new
  end
  
  def index
    @users = User.where(activated: true).paginate(page: params[:page], per_page: 7)
  end
  
  def show 
    @user = User.find(params[:id])
    redirect_to root_url and return unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page], per_page: 5)
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      #deixa o usuário já logado 
      #log_in @user
      # Handle a succesful save
      #flash[:success] = "#{@user.name}, Welcome to the Sample App!"
      #redirect_to @user
      #redirect_to user_url(@user) # possui o mesmo efeito do comando acima
      ## ^^ processo acima substituído quando necessária a ativação da conta
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      # Handle a successful update.
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private
  
    def user_params
      params.require(:user).permit( :name, :email, :password,
                                      :password_confirmation )
    end
    
    # Before filters
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user
    def admin_user
      if current_user
        redirect_to(root_url) unless current_user.admin?
      else
        redirect_to login_url
      end
    end
end