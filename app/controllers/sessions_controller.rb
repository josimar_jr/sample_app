class SessionsController < ApplicationController
  def new
  end
  
  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    #byebug
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        #Log the user in and redirect to the user's show page
        log_in @user
        #Evaluates if the checkbox to remember the user is checked
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        #Redirects the user to the user info page
        #redirect_to @user
        # Now redirects to the user info page only if doesn't try to get other info like edit for example
        redirect_back_or(@user)
      else
        message  = "Account not activated.  "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination!' #Not quite right
      render 'new'
    end
  end
  
  def destroy
    # only log the user out when logged in
    log_out if logged_in?
    redirect_to root_url
  end

end
