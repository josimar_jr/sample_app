class User < ApplicationRecord
  
  has_many :microposts, dependent: :destroy
  
  has_many :active_relationships, class_name:   "Relationship",
                                  foreign_key:  "follower_id",
                                  dependent:    :destroy
                                  
  has_many :passive_relationships, class_name:    "Relationship",
                                   foreign_key:   "followed_id",
                                   dependent:     :destroy
                                  
  has_many :following, through: :active_relationships, source: :followed
  #has_many :followers, through: :passive_relationships
  has_many :followers, through: :passive_relationships, source: :follower
  
  attr_accessor :remember_token, :activation_token, :reset_token
  ## método para realizar processo antes da inclusão / criação do registro
  before_create :create_activation_digest
  
  ## muda o conteúdo do atributo diretamente sem necessidade de self.email = valor
  before_save :downcase_email
  
  ## verifica a existência de conteúdo atravpes de String.blank? e a qtde de caracteres
  validates :name, presence: true, length: { maximum: 50 }
  
  ## verifica a existência de conteúdo atravpes de String.blank?, a qtde de caracteres
  ## se o formato de email é valido e se o conteúdo é único!
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                                    format: { with: VALID_EMAIL_REGEX },
                                    uniqueness: { case_sensitive: false }
  
  ## verificação da senha
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remembers a user in the database for use in persistent sessions
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest
  def authenticated?( attribute, token)
    # utiliza a concatenação de símbolo com atributo para pesquisar o valor do item
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  # Forgets a user
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Activates an account.
  def activate
    #update_attribute(:activated, true)
    #update_attribute(:activated_at, Time.zone.now)
    # pq está dentro do modelo de usuário (user.rb) não precisa inserir self.update_columns
    update_columns( activated: true, activated_at: Time.zone.now )
  end
  
  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  # Sets the password reset attributes
  def create_reset_digest
    self.reset_token = User.new_token
    #update_attribute(:reset_digest, User.digest(reset_token))
    #update_attribute(:reset_sent_at, Time.zone.now)
    update_columns( reset_digest: User.digest(reset_token),
                    reset_sent_at: Time.zone.now )
  end
  
  #Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  # Defines a proto-feed
  # See "Following users" for the full implementation.
  def feed
    # microposts 
    # this command above should have the same result, 
    # how does the rails framework find the correct microposts for the user?
    
    #Micropost.where("user_id = ?", id) # do not have the following user's micropost
    
    # rails considera o modelo User para chamar User.following_ids
    # que é a mesma coisa que User.following.map(&:id) extraindo somente o id da lista de objetos
    
    # Modelo usando diferentes códigos para identificação das variáveis na query
    #Micropost.where( "user_id = :user_id OR user_id IN (:following_ids) ", 
    #                following_ids: following_ids, user_id: id )
    
    # Modelo concatenando códigos de SQL para melhor performance
    following_ids = "SELECT followed_id FROM relationships 
                      WHERE follower_id = :user_id"
    Micropost.where( "user_id = :user_id OR user_id IN (#{following_ids}) ", user_id: id )
  end
  
  # Follows a user
  def follow( other_user )
    following << other_user
  end
  
  # Unfollows a user
  def unfollow( other_user )
    following.delete( other_user )
  end
  
  # Returns true if the current user is following the other user
  def following?(other_user)
    following.include?( other_user )
  end
  
  
  private
    # Converts email to all lower-case.
    def downcase_email
      #self.email = email.downcase
      email.downcase!
    end
    
    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token   = User.new_token
      self.activation_digest  = User.digest(activation_token)
    end
end
