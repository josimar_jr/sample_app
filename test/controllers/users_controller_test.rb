require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
    @user       = users(:usuario_user1)
    @other_user = users(:usuario_ocivs)
  end
  
  test "should sign up" do
    get signup_path
    assert_response :success
    assert_select "title", "Sign up | #{@base_title}"
  end
  
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@user)
    patch user_path(@other_user), params: { user: { name: @other_user.name,
                                              email: @other_user.email } }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@user)
    assert_not @user.admin?
    patch user_path(@user), params: { 
                                    user: { password:       "aaa123",
                                            password_confirmation: "aaa123",
                                            admin: true } }
    # este trecho provavelmente está errado
    # assert_not @user.FILL_IN.admin?
    # está faltando algo em FILL_IN ^^
    assert_not @user.admin?
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@user)
    assert_no_difference 'User.count' do
      delete user_path(@other_user)
    end
    assert_redirected_to root_url
  end
  
  test "should redirect following when not logged in" do
    get following_user_path(@user)
    assert_redirected_to login_url
  end
  
  test "should redirect followers when not logged in" do
    get followers_user_path(@user)
    assert_redirected_to login_url
  end
  
end
