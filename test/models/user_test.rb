require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup 
    @user = User.new( name:"Exemplo usuario", email:"user@local.com",
                      password: "eu12345", password_confirmation: "eu12345")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "      "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "   "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51 
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accpet valid adresses" do
    valid_adresses = %w[maria@local.com jose@email.com natan@fool.com]
    valid_adresses.each do |val_add|
      @user.email = val_add
      assert @user.valid?, "#{val_add.inspect} deveria ser válido <blergh>"
    end
  end
  
  test "email validation should reject invalid adresses" do
    invalid_adresses = %w[jose@email,com maria_at_local.com joao@email@outroemail.com 
                          natan@email+email.com gabs@local..com]
    invalid_adresses.each do |inv_email|
      @user.email = inv_email
      assert_not @user.valid?, "#{inv_email.inspect} deveria ser inválido < =/ >"
    end
  end
  
  test "email adresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email should be saved in lower-case" do
    mixed_case_email = "FooBAR@local.COM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "password should be no blank " do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    user1 = users(:usuario_user1)
    ocivs = users(:usuario_ocivs)
    
    assert_not user1.following?( ocivs )
    
    user1.follow(ocivs)
    assert user1.following?( ocivs )
    assert ocivs.followers.include?( user1 )
    
    user1.unfollow( ocivs )
    assert_not user1.following?( ocivs )
  end
  
  test "feed should have the right posts" do
    example = users(:usuario_user1)
    ocivs = users(:usuario_ocivs)
    other = users(:usuario_user2)
    
    # Posts from followed user
    example.microposts.each do |post_following|
      assert other.feed.include?( post_following )
    end
    
    # Posts from the user itself
    other.microposts.each do |post_self|
      assert other.feed.include?( post_self )
    end
    
    # Posts from unfollwed user
    ocivs.microposts.each do |post_unfollowed|
      assert_not other.feed.include?( post_unfollowed )
    end
    
  end  
  
end
