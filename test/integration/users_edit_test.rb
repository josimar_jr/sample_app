require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:usuario_user1)
  end
  
  test "unsuccessful edit" do
    log_in_as (@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
    assert_template 'users/edit'
    assert_select 'div.alert-danger', :text => 'The form contains 4 errors.'
  end
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_url(@user)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  
  test "forwarding works just for the first time" do
    get edit_user_path(@user)
    # Verifies the forwarding url isn't nil
    assert session[:forwarding_url]
    # Logs as the user
    log_in_as(@user)
    # Verifies the page is redirected to the user edit path
    assert_redirected_to edit_user_url(@user)
    # Logs the user out
    delete logout_path
    # Verifies the forwarging url is nil
    assert_nil session[:forwarding_url]
    # Logs as the user again
    log_in_as(@user)
    # Verifies the page is redirected to the user show page
    assert_redirected_to @user
  end
  
end
