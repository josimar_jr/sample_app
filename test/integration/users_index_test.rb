require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  
  def setup 
    @admin = users(:usuario_ocivs)
    @non_admin = users(:usuario_user1)
    ActionMailer::Base.deliveries.clear
  end
  
  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', count: 2
    first_page_of_users = User.paginate(page: 1, per_page: 7)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end
  
  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end
  
  test "new user should not be showed" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: {  name: "Test User",
                                          email: "test@example.com",
                                          password:              "password",
                                          password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    new_user = assigns(:user)
    assert_not new_user.activated?
    get login_path
    log_in_as(@admin)
    assert is_logged_in?
    get user_path(new_user)
    assert_redirected_to root_url
  end
  
  test "new user should not appear on index" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: {  name: "Test User",
                                          email: "test@example.com",
                                          password:              "password",
                                          password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    new_user = assigns(:user)
    assert_not new_user.activated?
    get login_path
    log_in_as(@admin)
    assert is_logged_in?
    get users_path
    last_page_of_users = User.paginate(page: 15, per_page: 7)
    last_page_of_users.each do |user|
      assert_not user == new_user
    end
  end
  
end
