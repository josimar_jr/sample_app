require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:usuario_ocivs)
    @exemplo1 = users(:usuario_user1)
    @exemplo2 = users(:usuario_user2)
  end
  
  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'h1>img.gravatar'
    assert_match @user.microposts.count.to_s, response.body
    assert_select 'div.pagination', count: 1
    # Verifica se há o trecho dos status de seguidores e seguidos
    assert_select 'div.stats'

    #following = @user.following.count
    #assert_select "following #{following}"

    #followers = @user.followers.count
    #assert_select "#{followers} followers"

    @user.microposts.paginate(page: 1, per_page: 5).each do |micropost|
      assert_match micropost.content, response.body
    end
  end

  test "button follow and unfollow on profiles" do
    get login_path
    post login_path, params: { session: { email: @user.email ,
                                        password: 'aaa123' } }
    assert is_logged_in?
    # não precisa mais depois de ter criado isto no fixtures
    #@user.follow @exemplo1
    get user_path(@exemplo1)
    assert_select "input[type=submit][value='Unfollow']"
    
    get user_path(@exemplo2)
    assert_select "input[type=submit][value='Follow']"

  end
  
end
