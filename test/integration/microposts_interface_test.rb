require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:usuario_user1)
  end
  
  test "micropost interface" do
    log_in_as(@user)
    assert is_logged_in?
    get root_path
    
    # não funcionou pois não adicionou paginação
    #assert_select 'div.pagination'
    
    # verifica a existência dos últimos 5 microposts
    # na página
    last5 = @user.microposts.take(5)
    #byebug
    last5.each do |micro|
      # gera problemas quando existir posts com acentuação ou comandos que conflitam com 
      # comandos de marcação do html > < : ' " é á É
      #assert_match micro.content, response.body unless micro.content.nil?
      assert_match CGI::escapeHTML(micro.content), response.body unless micro.content.nil?
    end
    assert_select 'input[type=file]'
    
    # Invalid submission
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "" } }
    end
    assert_select 'div#error_explanation'
    
    # Valid submission
    content = "This micropost really ties the room togheter"
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    assert assigns(:micropost).picture?
    assert_redirected_to root_url
    follow_redirect!
    
    assert_match content, response.body
    
    # Delete post
    assert_select 'a', text: 'delete'
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    
    # Visit differente user (no delete links)
    get user_path(users(:usuario_ocivs))
    assert_select 'a', text: 'delete', count: 0
  end
  
  test "micropost sidebar count" do
    log_in_as(@user)
    assert is_logged_in?
    
    get root_path
    assert_match "micropost".pluralize( @user.microposts.count), response.body
    
    # User with zero micropost
    other_user = users(:user_25)
    log_in_as(other_user)
    assert is_logged_in?
    
    get root_path
    assert_match "micropost".pluralize( other_user.microposts.count), response.body
    
    other_user.microposts.create!(content: "a new micropost")
    get root_path
    assert_match "micropost".pluralize( other_user.microposts.count), response.body
  end
end
