# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# Examples:
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!( name: "Rails Tutorial",
              email: "example@railstutorial.org",
              password: "aaa123",
              password_confirmation: "aaa123",
              admin: false,
              activated: true,
              activated_at: Time.zone.now )

User.create!( name: "Ócivs",
              email: "junior.ocivs@gmail.com",
              password: "aaa123",
              password_confirmation: "aaa123",
              admin: true,
              activated: true,
              activated_at: Time.zone.now )

User.create!( name: "Other User",
              email: "other@example.com",
              password: "aaa123",
              password_confirmation: "aaa123",
              admin: true,
              activated: true,
              activated_at: Time.zone.now )

99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "aaa123"
  User.create!( name: name,
                email: email,
                password: password,
                password_confirmation: password,
                activated: true,
                activated_at: Time.zone.now )
end

#Micropost
users = User.order(:created_at).take(6)
50.times do
  users.each { |user| user.microposts.create!(content: Faker::Lorem.sentence(word_count: 3, supplemental: true, random_word_to_add: 4)) }
end

# Following Relationships
users = User.all
exemplo = User.first
ocivs = User.second

# usuario exemplo
following = users[15..55]
following.each {|followed| exemplo.follow( followed ) }

followers = users[6..95]
followers.each {|follower| follower.follow( exemplo ) }

# usuario ocivs
ocivs.follow( exemplo )
exemplo.follow( ocivs )
users[3..99].each {|follower| follower.follow( ocivs ) }
